export function addTodo (item:object){
    return {
       type:"addTodo",
       payload:item
    }
}
export function deleteTodo (deleteId:object){
   return {
      type:"deleteTodo",
      payload:{
       deleteId
      }
   }
}
export function isLoder (){
   return {
       type:"isLoaderToTrue"
   }
}
export function added (){
   return {
       type:"isLoaderTofalse"
   }
}
export function userInput(value:string)){
   return {
       type:"userInput",
       payload:value
   }
}
export function reset(){
   return {
       type:"resetTodo",
       payload:{}
   }
}
export function handleEditTodo (data:object){
   return {
      type:"handleEditTodo",
      payload:{
       
       data
       
      }
   }
}
export function updateTodo(todo:string){
   return{
       type:"updateTodo",
       payload:{
           value:todo
       }
   }
}
export function setFilterType(values:string){
   return {
       type:"setFilterType",
       payload:values
   }
}
export function saveTodo (data:object,updateValue:string){
   return {
      type:"saveTodo",
      payload:{
       data,
       updateValue
      }
   }
}
export function handleCompleteTodo (data:object){
   return {
      type:"handleCompleteTodo",
      payload:{
       data,
      
      }
   }
}
export function handleClearTodo (){
   return {
      type:"handleClearTodo",
      
   }
}
/* middle-wares */ 
export function asyncAdd(item:object){
   return (dispatch)=>{
       dispatch(isLoder())
       setTimeout(()=>{
           dispatch(addTodo(item))
           dispatch(added())
       },300)
     
   }
}
