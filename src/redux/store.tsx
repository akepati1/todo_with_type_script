import { legacy_createStore as createStore,applyMiddleware} from 'redux' 
import {todoReducer} from "./reducer";
import thunk from 'redux-thunk'

const store = createStore(todoReducer:object, applyMiddleware(thunk));

export type RootState = ReturnType<typeof store.getState>;
export default store;
