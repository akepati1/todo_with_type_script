import React from "react";
import { useState } from "react";
import "../elements.css";
import { useSelector, useDispatch } from "react-redux";
import * as actions from "../redux/actions";
function Element(props) {
 const dispatch = useDispatch();
  const updateValue = useSelector(state=>state.updateValue)
  const { id, name, isCompleted } = props.data || {};
  const { toggle, editValue, onDelete, data } = props;
  return (
    <div className="container">
      <div
        onClick={() => toggle(data)}
        className={isCompleted === true ? "check-completed" : "check"}
      ></div>

      <li className={isCompleted === true ? "strick" : ""}>
        {id != editValue ? (
          name
        ) : (
          <input
            className="input editInput"
            type="text"
            value={updateValue}
            onChange={(e)=>dispatch(actions.updateTodo(e.target.value))}
          ></input>
        )}
      </li>

      <button
        style={{ border: "none" }}
        name="edit"
        className={id === editValue ? "save" : "edit"}
        onClick={()=>dispatch(actions.handleEditTodo(data))}
      >
        {id === editValue ? "SAVE" : "EDIT"}
      </button>
      <div onClick={() => onDelete(id)}>X</div>
    </div>
  );
}

export default Element;
